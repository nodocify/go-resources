# Golang Libraries

Python Requests => [net/http](https://golang.org/pkg/net/http/)
>The built in `net/http` provides HTTP client and server implementations that is really great, and super easy to use.

Python Flask + Jinja2 => [Gin](https://github.com/gin-gonic/gin)
>`Gin` is a web framework written in Go (Golang). It features a martini-like API with performance that is up to 40 times faster thanks to httprouter. If you need performance and good productivity, you will love Gin.

CLI Creation => [Cobra](https://github.com/spf13/cobra)
>`Cobra` is both a library for creating powerful modern CLI applications as well as a program to generate applications and command files.

Config => [Viper](https://github.com/spf13/viper)
>Viper is a complete configuration solution for Go applications including 12-Factor apps. It is designed to work within an application, and can handle all types of configuration needs and formats.

Windows Svc => [go-windows-svc](https://github.com/billgraziano/go-windows-svc)
>This is based on the GO Windows service example program provided by the GO Project. It is a project shell to create a Windows service.

# Extras
https://github.com/avelino/awesome-go

[Standard Lib](https://golang.org/pkg/)

# Tips
[What’s the point of GoLang Pointers? Everything you need to know!](https://medium.com/@annapeterson89/whats-the-point-of-golang-pointers-everything-you-need-to-know-ac5e40581d4d)
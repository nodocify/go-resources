package main

import (
	"flag"
	"io/ioutil"
	"os"

	"github.com/google/logger"
)

const logPath = "example.log"

var verbose = flag.Bool("verbose", false, "print info level logs to stdout")

func main() {

	// parse command line flags
	flag.Parse()

	// opens our logfile, create it if it doesn't exist, in write only, append mode
	lf, err := os.OpenFile(logPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0660)
	if err != nil {
		logger.Fatalf("Failed to open log file: %v", err)
	}
	defer lf.Close()

	// There are 4 arguments here.
	// 1. Name: This is the name of what gets put into system logs (ie, event viewer on windows and journalctl on linux)
	// 2. Stdout: This is a bool whether to output to stdout.
	// 3. systemlog: This is a bool whether to output to system log.
	// 4. logfile: This is logfile to output to. Must be an io.Writer type.
	loggerOne := logger.Init("LoggerExample", *verbose, true, lf)
	defer loggerOne.Close()

	loggerTwo := logger.Init("LoggerExample", true, false, ioutil.Discard)
	defer loggerTwo.Close()

	loggerOne.Info("This will log to the log file and system log but not stdout")
	loggerOne.Error("This is an error.")
	loggerTwo.Info("This will only log to stdout")
	logger.Info("This is the same as using loggerOne.")

}
